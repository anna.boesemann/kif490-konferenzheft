SHELL = bash
.SECONDARY:

BUILD_DIR = build/

TEX_FILES = $(shell find . -type f -name "*.tex")
CLS_FILES = $(shell find . -type f -name "*.cls")
STY_FILES = $(shell find . -type f -name "*.sty")
PDFLATEX_ARGS = -interaction=nonstopmode -shell-escape \
	-file-line-error -halt-on-error
PDFLATEX = cd src \
	&& export TEXMFHOME="$(shell pwd)/src/texmf" \
	&& pdflatex $(PDFLATEX_ARGS)

SVG_DIR = graphics/svg/
SVG_BUILD_DIR = $(BUILD_DIR)svg/
SVG_FILES = $(shell find $(SVG_DIR) -type f -name "*.svg" -exec basename {} \; )
SVG_PDFS = $(addprefix $(SVG_BUILD_DIR),$(addsuffix _svg.d,$(basename $(SVG_FILES))))
SVG_SKIP = Rueckseite book

JPG_FILES = $(shell find graphics/ -type f -name "*.jpg")
PNG_FILES = $(shell find graphics/ -type f -name "*.png")

PDF_DEPENDS = $(TEX_FILES) $(CLS_FILES) $(STY_FILES) $(JPG_FILES) $(PNG_FILES)

all: paper online
paper online: %: $(BUILD_DIR)%/main.pdf

$(BUILD_DIR)paper/ $(BUILD_DIR)online/:
	@mkdir -p $@

$(SVG_BUILD_DIR)%_svg.d: $(SVG_DIR)%.svg
	$(eval SVG_FILE = $(SVG_DIR)$*.svg)
	$(eval PDF_FILE = $(SVG_BUILD_DIR)$*_svg.pdf)
	@mkdir -p $(SVG_BUILD_DIR)
	@echo -e -n "$$(realpath --relative-to=. $(SVG_FILE))"
	@if echo "$(SVG_SKIP)" | grep -v -w $* &> /dev/null; then \
		inkscape -z -C -T --export-pdf=$(PDF_FILE) $(SVG_FILE) &>/dev/null; \
		echo -e " -> $$(realpath --relative-to=. $(PDF_FILE))"; \
	else \
		echo -e " skipped"; \
	fi
	@touch $@
.PRECIOUS: $(SVG_BUILD_DIR)%_svg.pdf

$(BUILD_DIR)%/main.pdf: \
	$(BUILD_DIR)%/ \
	$(SVG_PDFS) \
	$(PDF_DEPENDS)
	@echo -e -n "src/main.tex"
	@output=$$($(PDFLATEX) -draftmode -output-directory ../$(BUILD_DIR)$* \
		"\newcommand{\targetOutput}{$*} \input{main}") \
		|| (echo -e "\n\n$${output}\n\n" && exit 1)
	@output=$$($(PDFLATEX) -output-directory ../$(BUILD_DIR)$* \
		"\newcommand{\targetOutput}{$*} \input{main}") \
		|| (echo -e "\n\n$${output}\n\n" && exit 1)
	@cp -f "$(BUILD_DIR)$*/main.pdf" "$*_Heft.pdf"
	@echo -e " -> $*_Heft.pdf"

clean:
	@rm -rf $(BUILD_DIR)
