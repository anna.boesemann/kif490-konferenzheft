\ProvidesClass{GAU}[2016/06/01 v0.1 Informationsheftklasse_Info_GAU von patalema]
%paper format, textsize, page-setting
\PassOptionsToClass{a5paper,10pt,twoside}{tyoearea}
\LoadClassWithOptions{scrartcl}
\ProcessOptions\relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Packages			%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%% language and text encoding %%%%%%%%%%%
\RequirePackage[utf8]{inputenc}	% encoding der zeichen, depends on operating system
\RequirePackage[T1]{fontenc}	% enables using ae,oe,ue with points
\RequirePackage[english,ngerman]{babel}	% new german spelling

%package by patalema with special commands for including directory specific content
% * differs in online=1 and paper=0 
% * 2021/01/21 minor change: include SelectOutput instead of MI
\RequirePackage{SelectOutput}

%%%%%%%%%% other %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%enables setting of margins and more
\ifpaper
\RequirePackage[papersize={154mm,216mm},foot=20mm,left=17mm,right=17mm, top=8mm, bottom=23mm]{geometry}
\else
\RequirePackage[papersize={148mm,210mm},foot=17mm,left=14mm,right=14mm, top=8mm, bottom=20mm]{geometry}
\fi
%\RequirePackage{microtype}
%\RequirePackage{inconsolata}
%package with special commands for KIF-Konferenzheft styling
% * 2021/04/18
\RequirePackage{KIFstyle}
%enables using colors
\RequirePackage{xcolor}
%enables some fancy free-defined tables
\RequirePackage{tabularx}
%enables multicolumns
\RequirePackage{multicol}
\setlength{\columnsep}{.5cm}
\RequirePackage{setspace}
%some list package for listings %doro
\RequirePackage{enumitem}
\RequirePackage{wasysym}
%timetable ophase Packages
\RequirePackage{tabularx}
\RequirePackage{lscape}
\RequirePackage{timetable}
\RequirePackage{graphicx}
\RequirePackage[absolute]{textpos}

%enabling use of euro sign
\RequirePackage{eurosym}
%using subfigure environment
\RequirePackage[textfont=small,labelfont=small]{caption}
\RequirePackage{wrapfig}
\RequirePackage{pgfkeys}
%for cutting edge
\RequirePackage{tikz}
\usetikzlibrary{shapes.geometric}

%letztes packet!
\RequirePackage{hyperref}
\RequirePackage{amssymb}
\RequirePackage{ascii}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		Commands			%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% set path-variables
\newcommand{\srcDir}{.}
\newcommand{\contents}{../contents}
\newcommand{\graphics}{../graphics}
\newcommand{\includesvg}[2][]{\includegraphics[#1]{../build/svg/#2_svg.pdf}}
%def um zielordner der suche anzugeben
\newcommand{\content}{0}
\newcommand{\final}{1}

%including own title/envelope
\newcommand{\inputtitle}[1]{\input{#1}}
%platzbefehl für den Zeitplan
\newcommand{\spacy}{\ifpaper \vspace*{0.30cm} \else \vspace*{0.38cm} \fi}
%some def for section ToDO %doro
\def\el@CheckList{checklist}  
%no indent
\setlength{\parindent}{0pt}
\setitemize{leftmargin=*} 

\pgfkeys{
    /xkcdInput/.is family,
    /xkcdInput,
        default/.style = {
             width = \textwidth,
             urlxoffset = -10cm,
             urlyoffset = 0pt
        },
        width/.estore in = \xkcdInputWidth,
        urlxoffset/.estore in = \xkcdUrlxOffset,
        urlyoffset/.estore in = \xkcdUrlyOffset,
    /portraitInput/.is family,
    /portraitInput,
        default/.style = {
            width = 0.29\textwidth,
        },
        width/.estore in = \portraitInputWidth
}
\newcommand{\portraitInput}[3][]{
    \pgfkeys{/portraitInput, default, #1}
    \begin{minipage}{0.5\textwidth}
    \centering\includegraphics[width=\portraitInputWidth]{\graphics/portrait/#2}\\
    \end{minipage}

}
\newcommand{\coverInput}[1]{
    \includegraphics[width=1\paperwidth]{\graphics/cover/#1}
}
\newcommand{\xkcdInput}[2][]{
    \pgfkeys{/xkcdInput, default, #1}
    \begin{center}
        \includegraphics[width=\xkcdInputWidth]{\graphics/xkcd/#2}\\[\xkcdUrlyOffset]
        \raisebox{0pt}{\hspace*{\xkcdUrlxOffset}
            \small{[\href{https://xkcd.com}{xkcd.com}]}}
    \end{center}
}

% switch-case
\newcommand{\ifequals}[3]{\ifthenelse{\equal{#1}{#2}}{#3}{}}
\newcommand{\case}[2]{#1 #2}
\newenvironment{switch}[1]{\renewcommand{\case}{\ifequals{#1}}}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		InDocument			%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\AtBeginDocument{
  \pagestyle{empty}
}

