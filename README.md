# Konferenzheft KIF490

Die aktuellste Online-PDF aus dem Master-branch (current status) befindet sich [hier](https://event.kif.rocks/heft.pdf).

## Was ist das? 

Dies ist das Konferenzheft zur 49,0.ten Konferenz deutschsprachiger Informatikfachschaften, die im Frühjahr/Sommer 2021 in Göttingen stattfindet.
Das Heft basiert auf dem Layout des Erstsemesterinformationsheftes des Fachschaftsrats Mathematik und Informatik und den Fachgruppen der Fakultät der Uni Göttingen.

## Vorraussetzungen

Es ist empfohlen texlive-full zu installieren, da diverse LaTeX-Pakete verwendet werden.

    sudo apt-get install build-essential texlive-full inkscape

## Kompilieren der .tex-Dateien

Die druckfertigen Dateien für die verschiedenen Heftversionen (\*_Heft.pdf) lassen sich durch Ausführung des folgenden Kommandos erstellen:

    make

Außerdem sind folgende Alternativen möglich:

    make paper # Kompiliert das Konferenzheft nur im druckfähigen Format
    make online # Kompiliert das Konferenzheft in einem für die digitale Herausgabe optimierten Format

    make clean # Löscht die Daten die beim Kompilieren erzeugt wurden (ausgenommen der fertigen Hefte)

## Autodeployment

Unter [https://event.kif.rocks/heft.pdf](https://event.kif.rocks/heft.pdf) findet man ab jetzt die aktuelle (`master`) Online-Version von diesem Heft.

Es gibt keinerlei Garantie, dass diese URL nach der KIF noch funktioniert. (Wird vllt. noch einige Monate funktionieren. Zur nächsten KIF aber wahrscheinlich nicht mehr.)

Falls ein anderer Branch fürs Deployment verwendet werden soll, bitte mich (Jake) anschreiben.


